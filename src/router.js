import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      alias: "/tutorials",
      name: "tutorials",
      component: () => import("./components/TutorialsList")
    },
    {
      path: "/tutorials/:id",
      name: "tutorial-details",
      component: () => import("./components/Tutorial")
    },
    {
      path: "/add",
      name: "add",
      component: () => import("./components/AddTutorial")
    }, 
    {
      path: "/table",
      name: "table",
      component: () => import("./components/TutorialTable")
    }, 
    {
      path: "/table2",
      name: "table2",
      component: () => import("./components/TutorialTable2")
    }, 
    {
      path: "/tableB",
      name: "tableB",
      component: () => import("./components/TutorialTableB")
    }
  ]
});
