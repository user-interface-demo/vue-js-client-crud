# Vue.js CRUD App with Vue Router & Axios

Code is based on this example: https://github.com/bezkoder/vue-js-client-crud

Start app first: https://gitlab.com/spring-utils/spring-mysql

## Run local

### Install node.js, npm and vue

Install node.js version 14.x.x

```bash
curl -sL https://deb.nodesource.com/setup_14.x | sudo bash -
sudo apt install nodejs -y
```

Test it with

```bash
node -v
```

Test npm

```bash
npm -v
```

Install vue

```bash
sudo npm install -g @vue/cli-service-global
```

Test with

```bash
vue -V
```

### Run local

First start spring project for example `spring-boot-data-jpa-mysql`

First run install of npm

```
npm install
```

then start service

```
npm run serve
```

## Development

### Vue-Table

Example comes from here https://xaksis.github.io/vue-good-table/guide/advanced/

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

